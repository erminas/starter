import {Store, StoreConfig} from '@datorama/akita';
import {Injectable} from '@angular/core';

export interface AuthState {
    isLoggedIn: boolean;
    accessToken: string | null;
}

export function createInitialState(): AuthState {
    return {
        isLoggedIn: false,
        accessToken: null,
    };
}

@Injectable({
    providedIn: 'root',
})
@StoreConfig({ name: 'auth' })
export class AuthStore extends Store<AuthState> {
    constructor() {
        super(createInitialState());
    }
}
