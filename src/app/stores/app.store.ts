import {Store, StoreConfig} from '@datorama/akita';
import {Injectable} from '@angular/core';
import {AvailableLanguages} from '../models/types/available-locales';
import {getBrowserLanguageShort} from '../util/browser-language';

export interface AppState {
    language: AvailableLanguages;
    test: string;
}

export function createInitialState(): AppState {
    return {
        language: getBrowserLanguageShort(),
        test: "hi"
    };
}

@Injectable({
    providedIn: 'root',
})
@StoreConfig({ name: 'app' })
export class AppStore extends Store<AppState> {
    constructor() {
        super(createInitialState());
    }
}
