import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ErminasDialogService} from '@erminas/erminas-dialog';
import {TranslateService} from '@ngx-translate/core';
import {TranslationKey} from '../../models/types/translation-key';

@Injectable({
    providedIn: 'root'
})
export class DialogService {

    constructor(
        private matDialog: MatDialog,
        private erminasDialogService: ErminasDialogService,
        private translateService: TranslateService,
    ) {
    }

    showError(errMsgKey: TranslationKey, errorObject?: Error, errMsgParameters?: any, actionKey: TranslationKey = 'BUTTON.OK'): void {
        const message = this.translateService.instant(errMsgKey, errMsgParameters);
        if (errorObject) {
            console.error(message, errorObject);
        }
        this.erminasDialogService.showToast(message, 5000, this.translateService.instant(actionKey));
    }

    showSuccess(errMsgKey: TranslationKey, errMsgParameters?: any): void {
        const msg = this.translateService.instant(errMsgKey, errMsgParameters);
        this.erminasDialogService.showToast(msg);
    }

    showInfo(errMsgKey: TranslationKey, errMsgParameters?: any): void {
        const msg = this.translateService.instant(errMsgKey, errMsgParameters);
        this.erminasDialogService.showToast(msg);
    }

    showDialog(errMsgKey: TranslationKey, errMsgParameters?: any): void {
        const msg = this.translateService.instant(errMsgKey, errMsgParameters);
        this.erminasDialogService.showDialog(msg);
    }
}
