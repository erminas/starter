import {Injectable} from '@angular/core';
import {AuthStore} from '../../stores/auth.store';
import {ApiAuthorizationService} from '../../api/services/api-authorization.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    /**
     * This file is used to update the store values
     *
     * @param store The Akita Auth Store
     * @param authService The Api Authorization Service (Swagger)
     */
    constructor(
        private store: AuthStore,
        private authService: ApiAuthorizationService,
    ) {
    }

    async signIn(username: string, password: string): Promise<void> {
        try {
            const openIdToken = await this.authService.connectTokenPost({ body: { password, username, grant_type: 'password', scope: 'offline_access' } })
                .toPromise();
            this.store.update({
                accessToken: openIdToken.access_token,
                isLoggedIn: true
            });
        } catch (err) {
            console.error(err);
            this.signOut()
                .then();
        }
    }

    async signOut(): Promise<void> {
    }

}
