import {Inject, Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {DOCUMENT} from '@angular/common';
import {AppStore} from '../../stores/app.store';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    private loadingCounter = new BehaviorSubject<number>(0);

    constructor(
        private store: AppStore,
        @Inject(DOCUMENT) private document: Document,
    ) {

        this.initLoadingSpinner();
    }

    showSpinner(): void {
        const counter = this.loadingCounter.getValue();
        this.loadingCounter.next(counter + 1);
    }

    hideSpinner(): void {
        const counter = this.loadingCounter.getValue();
        this.loadingCounter.next(Math.max(0, counter - 1));
    }

    private initLoadingSpinner(): void {
        this.loadingCounter.subscribe((isLoading: number) => {
            const elem = this.document.querySelector('#loading-spinner');
            if (!elem) {
                return;
            }
            if (isLoading > 0) {
                elem.classList.add('show');
            } else {
                elem.classList.remove('show');
            }
        });
    }
}
