import {Query} from '@datorama/akita';
import {Injectable} from '@angular/core';
import {AuthState, AuthStore} from '../stores/auth.store';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class AuthQuery extends Query<AuthState> {

    isLoggedIn$: Observable<boolean> = this.select('isLoggedIn');

    /**
     * This file is used to query the store
     *
     * @param store
     */
    constructor(protected store: AuthStore) {
        super(store);
    }

    getIsLoggedIn(): boolean {
        return this.store.getValue().isLoggedIn;
    }
}
