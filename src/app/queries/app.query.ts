import {Query} from '@datorama/akita';
import {AppState, AppStore} from '../stores/app.store';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class AppQuery extends Query<AppState> {
    currentLanguage$: Observable<string> = this.select('language');

    constructor(protected store: AppStore) {
        super(store);
    }

    getLanguage(): string {
        return this.store.getValue().language;
    }
}
