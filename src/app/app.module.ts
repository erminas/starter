import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AppService} from './services/app/app.service';
import {environment} from '../environments/environment';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {translateHttpLoaderFactory} from './util/translate-http-loader-factory';
import {HTTP_INTERCEPTORS, HttpBackend, HttpClientModule} from '@angular/common/http';
import {appInitializerFactory} from './util/app-initializer';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './components/login/login.component';
import {ErrorComponent} from './components/error/error.component';
import {ErminasDialogModule} from '@erminas/erminas-dialog';
import {ApiModule} from './api/api.module';
import {AuthInterceptor} from './interceptors/auth.interceptor';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        ErrorComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        TranslateModule.forRoot({
            defaultLanguage: 'de',
            loader: {
                provide: TranslateLoader,
                useFactory: translateHttpLoaderFactory,
                deps: [HttpBackend]
            }
        }),
        ApiModule.forRoot({ rootUrl: environment.apiUrl }),
        environment.production ? [] : AkitaNgDevtools.forRoot({ name: 'starter' }),
        BrowserAnimationsModule,
        ErminasDialogModule.forRoot({
            snackbarDuration: 3000
        }),
        MatButtonModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        {
            provide: APP_INITIALIZER,
            useFactory: appInitializerFactory,
            deps: [TranslateService],
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(
        private appService: AppService,
    ) {
    }
}
