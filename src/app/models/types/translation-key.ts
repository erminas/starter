import translationsJson from '../../../assets/i18n/en.json';

type ValueOf<T> = T[keyof T]
type TranslationPaths<T, Path extends string = ''> = ValueOf<{
    [K in keyof T & string]:
    T[K] extends string
    ? K
    : (`${Path}${K}` | `${Path}${K}.${TranslationPaths<Extract<T[K], T[K]>>}`)
}>
export type TranslationKey = TranslationPaths<typeof translationsJson>
