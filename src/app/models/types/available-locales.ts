// eslint-disable-next-line
export const DefaultLanguage = 'en';
// eslint-disable-next-line
export const Languages = ['de', 'en'] as const;
type ElementTypes<T extends ReadonlyArray<unknown>> = T extends ReadonlyArray<infer ElementType> ? ElementType : never;
export type AvailableLanguages = ElementTypes<typeof Languages>;
// eslint-disable-next-line
export const Locales = ['de-DE', 'en-EN'] as const;
export type AvailableLocales = ElementTypes<typeof Locales>;
