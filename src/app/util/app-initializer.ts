import {getBrowserLanguageShort} from './browser-language';
import {AvailableLanguages} from '../models/types/available-locales';
import {TranslateService} from '@ngx-translate/core';

export function appInitializerFactory(
    translate: TranslateService,
): () => Promise<void> {
    return async () => {
        const langToSet: AvailableLanguages = getBrowserLanguageShort();
        translate.setDefaultLang(langToSet);
        try {
            await translate.use(langToSet)
                .toPromise();
            console.log(`Successfully initialized '${langToSet}' language.`);
            await translate.get('LANGUAGE')
                .toPromise();
        } catch (err) {
            console.error(`Problem with '${langToSet}' language initialization.`);
        }
    };
}
