import {HttpBackend, HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function translateHttpLoaderFactory(httpBackend: HttpBackend): TranslateHttpLoader {
    const http = new HttpClient(httpBackend);
    return new TranslateHttpLoader(http);
}
