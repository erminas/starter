import {AvailableLanguages, AvailableLocales, DefaultLanguage, Languages} from '../models/types/available-locales';

export function getBrowserLocale(): AvailableLocales {
    const language = getBrowserLanguageShort();
    return getLocaleByLanguage(language);
}

export function getLocaleByLanguage(languageShort: AvailableLanguages): AvailableLocales {
    switch (languageShort) {
        case 'de':
            return 'de-DE';
        case 'en':
            return 'en-EN';
        default:
            return 'en-EN';
    }
}

export function getBrowserLanguageShort(): AvailableLanguages {
    let navigatorLanguage = navigator.language.trim()
        .split(/-|_/)[0] || (navigator as any).userLanguage.trim()
        .split(/-|_/)[0];
    if (Languages.indexOf(navigatorLanguage) === -1) {
        navigatorLanguage = DefaultLanguage;
    }
    return navigatorLanguage;
}
