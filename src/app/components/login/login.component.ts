import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Observable} from 'rxjs';
import {AuthQuery} from '../../queries/auth.query';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    isLoggedIn$: Observable<boolean>;

    constructor(
        private authService: AuthService,
        private authQuery: AuthQuery,
    ) {
        this.isLoggedIn$ = this.authQuery.isLoggedIn$;
    }

    async ngOnInit(): Promise<void> {
        // when the design is done, remove this and call the signIn function from the form submit
        this.authService.signIn('demo1@test.invalid', 'RbHy6ygKeC#')
            .then();
    }

}
