## Todos

1. Implement the design for the login page (see below)
2. Use the login form to sign in with swagger (prepared signIn in ngOnInit)
    1. Fetch data in services (see akita documentation)
    2. Read/display data from queries (see akita documentation)
3. Fetch the user list from the swagger backend and display it on a new page after login
    1. Create a new component and navigate to it after successful login

## Angular Material

[Material Documentation](https://material.angular.io/components/categories)

## Backend

The Swagger for the backend is available on [Swagger Backend](https://diva-backend-dev.azurewebsites.net/swagger/index.html)

## Akita State Management

[Akita Documentation](https://datorama.github.io/akita/docs/store)

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

# Inspiration for the Login Page

Optional: Change the color to erminas default orange: `#ef7c00`

![Login](./src/assets/image/login.jpeg)
